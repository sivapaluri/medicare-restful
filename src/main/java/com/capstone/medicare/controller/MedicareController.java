package com.capstone.medicare.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.capstone.medicare.model.MedicareInventory;
import com.capstone.medicare.repository.InventoryRepository;

@RestController
@CrossOrigin

public class MedicareController {

	@Autowired
	InventoryRepository invrepo;

	@PostMapping("/adm-inv")
	public MedicareInventory createInventory(@RequestBody MedicareInventory medicareinventory) {

		return invrepo.save(medicareinventory);
	}

	@GetMapping("/adm-inv")
	public List<MedicareInventory> getAllInventory() {
		// TODO Auto-generated method stub
		return (List<MedicareInventory>) invrepo.findAll();
	}

	@GetMapping("/cs-inv")
	public List<MedicareInventory> getAllInventoryCustomer() {
		// TODO Auto-generated method stub
		return (List<MedicareInventory>) invrepo.findAll();
	}

	@GetMapping("/cs-invm/{type}")
	public List<MedicareInventory> getInventoryByName(@PathVariable("type") String type) {
		return invrepo.findByType(type);

	}

	@GetMapping("/adm-inv/{inventoryId}")
	public void deleteStockUnit(@PathVariable("inventoryId") Integer inventoryId) {
		// TODO Auto-generated method stub
		invrepo.findById(inventoryId);

	}

	@DeleteMapping("/adm-inv/{inventoryId}")
	public void MedicareInventory(@PathVariable("inventoryId") Integer inventoryId) {
		// TODO Auto-generated method stub
		invrepo.deleteById(inventoryId);

	}

	@PatchMapping("/adm-inv")
	public MedicareInventory updateInventory(@RequestBody MedicareInventory medicareinventory) {

		return invrepo.save(medicareinventory);
	}

}
