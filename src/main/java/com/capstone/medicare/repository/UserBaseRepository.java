package com.capstone.medicare.repository;

import org.springframework.data.repository.CrudRepository;

import com.capstone.medicare.model.MedicareUserBase;

public interface UserBaseRepository extends CrudRepository<MedicareUserBase, Integer> {

}
