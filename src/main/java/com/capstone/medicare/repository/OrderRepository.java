package com.capstone.medicare.repository;


import org.springframework.data.repository.CrudRepository;

import com.capstone.medicare.model.MedicareSalesOrder;

public interface OrderRepository extends CrudRepository<MedicareSalesOrder, Integer> {

}
