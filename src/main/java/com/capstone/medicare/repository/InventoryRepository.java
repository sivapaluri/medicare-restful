package com.capstone.medicare.repository;


import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.capstone.medicare.model.MedicareInventory;



public interface InventoryRepository extends CrudRepository<MedicareInventory, Integer> {
	
	public MedicareInventory findByMedicineName(String medicineName);
	public List<MedicareInventory> findByType(String type);



}
