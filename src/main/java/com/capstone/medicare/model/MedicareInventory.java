package com.capstone.medicare.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table
@Getter
@Setter
@NoArgsConstructor

public class MedicareInventory {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer inventoryId;
	private String medicineName;
	private int quantity;
	private double price;
	private String manufactureDate;
	private String expiryDate;
	private String batchNo;
	private String isEnabled;
	private String type;
	private String url;
	
	
	

}
