package com.capstone.medicare.repository;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

import com.capstone.medicare.model.MedicareInventory;
import com.capstone.medicare.repository.InventoryRepository;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class InventoryServiceTest {
	
	@Autowired
	InventoryRepository repo;

	@Test
	
	@Rollback(false)
	void testCreateInventory() {
		MedicareInventory medicareinventory = new MedicareInventory();
		MedicareInventory savedmedicareinventory = repo.save(medicareinventory);
		assertNotNull(savedmedicareinventory);

	}

	@Test
	void testGetAllInventory() {
		Iterable<MedicareInventory> medicareinventorylist = repo.findAll();
		assertNotNull(medicareinventorylist);
	}

//	@Test
//	void testGetAllInventoryByMedicineName() {
//		String name="Balm";
//		MedicareInventory medicareinventorylist =repo.findByMedicineName(name);
//		assertNotNull(medicareinventorylist);
//	}
//
//	@Test
//	void testGetAllInventoryByMfgDate() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	void testGetAllInventoryByExpDate() {
//		fail("Not yet implemented");
//	}
//
	@Test
	@Rollback(false)
	void testUpdateInventory() {
		fail("Not yet implemented");
	}
//
//	@Test
//	void testDeleteInventory() {
//		fail("Not yet implemented");
//	}

}
